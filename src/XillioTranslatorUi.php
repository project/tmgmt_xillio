<?php

namespace Drupal\tmgmt_xillio;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\tmgmt\Form\TranslatorForm;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Xillio translator UI.
 */
class XillioTranslatorUi extends TranslatorPluginUiBase {

  /**
   * The api connector.
   *
   * @var \Drupal\tmgmt_xillio\TapiccConnector
   */
  private $apiConnector;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * XillioTranslatorUI constructor.
   *
   * @param array $configuration
   *   The plugin config.
   * @param string $plugin_id
   *   The plugin id
   * @param mixed $plugin_definition
   *   The plugin definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->apiConnector = \Drupal::service('tmgmt_xilio.tapicc_connector');
    $this->requestStack = \Drupal::requestStack();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form_state_settings = $form_state->getUserInput()['settings'] ?? [];

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $settings = $translator->getSettings();

    $form['service_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LocHub API url'),
      '#default_value' => $settings['service_url'],
      '#description' => $this->t('The url of the translation instance without trailing slash.'),
      '#required' => TRUE,
      '#placeholder' => 'https://example.lochub.com',
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LocHub username'),
      '#default_value' => $settings['username'],
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LocHub password'),
      '#required' => TRUE,
      '#default_value' => $settings['password'],
    ];

    $form += parent::addConnectButton();

    // If connected, get projects.
    // Get settings from either the translator or form state.
    $available_settings = [];
    foreach (['username', 'password', 'service_url'] as $setting) {
      if (!empty($settings[$setting])) {
        $available_settings[$setting] = $settings[$setting];
      }
      elseif (!empty($form_state_settings[$setting])) {
        $available_settings[$setting] = $form_state_settings[$setting];
      }
    }

    if (count($available_settings) == 3) {
      $available = $translator->checkAvailable()->getSuccess();
      if (!$available) {
        $this
          ->messenger()
          ->addError($this->t('The provider is not available. Please check the settings or contact support.'));
      }

      try {
        $projects = $this->apiConnector
          ->setServiceUrl($available_settings['service_url'])
          ->setAuthToken($available_settings['username'], $available_settings['password'])
          ->getProjects();
      }
      catch (\Exception $exception) {
        $this
          ->messenger()
          ->addError($this->t('Error while fetching projects: @error', [
            '@error' => $exception->getMessage(),
          ]));
      }

      $project_options = [];
      if ($available && !empty($projects)) {
        foreach ($projects as $project) {
          $project_options[$project['id']] = $project['name'];
        }
        $form['project'] = [
          '#type' => 'select',
          '#title' => $this->t('Project'),
          '#description' => $this->t('Select the project used for this provider'),
          '#options' => $project_options,
          '#default_value' => $translator->getSetting('project'),
        ];
      }
    }

    // First check if the webhook matches with upstream.
    $webhook_synced = FALSE;
    if ($translator->getSetting('webhook_id')) {
      try {
        $upstream_webhook = $this->apiConnector->getWebhook($translator->getSetting('webhook_id'));

        if ($upstream_webhook['id'] != $translator->getSetting('webhook_id')) {
          throw \Exception($upstream_webhook['id'] . ' does not match ' . $translator->getSetting('webhook_id'));
        }

        $webhook_synced = TRUE;

      } catch (\Exception $exception) {
        $this->messenger()
          ->addError($this->t('The configured webhook does not match upstream (@error). Please re-create the webhook.', [
            '@error' => $exception->getMessage(),
          ]));
        $translator->setSetting('webhook_id', '')->save();
      }
    }

    $form['webhook_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook id'),
      '#default_value' => $translator->getSetting('webhook_id'),
      '#disabled' => TRUE,
    ];

    if ($translator->isNew()) {
      $form['webhook_id']['#description'] = $this->t('Webhook actions will be enabled after saving.');
    }

    if (isset($upstream_webhook) && $webhook_synced) {
      $form['last_message'] = [
        '#type' => 'container',
        '#prefix' => $this->t('Webhook status:'),
        '#attributes' => [
          'class' => ['last-message'],
        ],
        'status' => [
          '#theme' => 'item_list',
          '#items' => [
            ['#markup' => $this->t('Url @url', ['@url' => $upstream_webhook['url']])],
            ['#markup' => $this->t('Status: @status', ['@status' => $upstream_webhook['endpointStatus']])],
            ['#markup' => $this->t('Last message: @message', ['@message' => $upstream_webhook['endpointLastErrorMessage'] ?? $this->t('No messages')])],
          ]
        ],
      ];
    }

    $form['create_webhook'] = [
      '#type' => 'submit',
      '#value' => t('Create webhook'),
      '#disabled' => $translator->isNew(),
      '#submit' => [
        [$this, 'createWebhook'],
      ],
      '#limit_validation_errors' => [['settings']],
      '#executes_submit_callback' => TRUE,
    ];

    $form['delete_webhook'] = [
      '#type' => 'submit',
      '#value' => t('Delete webhook'),
      '#disabled' => $translator->isNew(),
      '#submit' => [
        [$this, 'deleteWebhook'],
      ],
      '#access' => !empty($translator->getSetting('webhook_id')),
      '#limit_validation_errors' => [['settings']],
      '#executes_submit_callback' => TRUE,
    ];

    return $form;
  }

  /**
   * Handles submit call of "Create webhook" button.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function createWebhook(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $payload['projectIds'] = [$translator->getSetting('project')];
    $result = $this->apiConnector->createWebhook($payload);
    if (isset($result['id'])) {
      $translator->setSetting('webhook_id', $result['id']);
      $translator->save();
    }

    $options = [];
    $query = $this->requestStack->getCurrentRequest()->query;
    if ($query->has('destination')) {
      $options['query']['destination'] = $query->get('destination');
      $query->remove('destination');
    }
    $form_state->setRedirect('<current>', [], $options);
  }

  /**
   * Handles submit call of "Dekete webhooks" button.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteWebhook(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    if ($webhook_id = $translator->getSetting('webhook_id')) {
      $this->apiConnector->deleteWebhook($webhook_id);

      $translator->setSetting('webhook_id', '');
      $translator->save();
    }

    $options = [];
    $query = $this->requestStack->getCurrentRequest()->query;
    if ($query->has('destination')) {
      $options['query']['destination'] = $query->get('destination');
      $query->remove('destination');
    }
    $form_state->setRedirect('<current>', [], $options);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $settings = $form_state->getValue('settings');

    // Validate trailing slashes.
    if (substr($settings['service_url'], -1) == '/') {
      $form_state->setErrorByName('settings',$this->t('The api url should not end with a trailing slash.'));
    }

    try {
      $result = $this->apiConnector
        ->setServiceUrl($settings['service_url'])
        ->getAuthToken($settings['username'], $settings['password'], TRUE);
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('settings',$this->t($e->getMessage()));
    }

    if (!$result) {
      $form_state->setErrorByName('settings',$this->t('Connection to the service provider failed, please check url and credentials.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $settings['due_at'] = [
      '#type' => 'datetime',
      '#title' =>$this->t('Due at'),
      '#description' =>$this->t('Enter the due date for those translations'),
      '#default_value' => $job->getSetting('due_at') ? $job->getSetting('due_at') : '',
      '#required' => TRUE,
      '#element_validate' => [[static::class, 'validateDueAt']],
    ];

    $settings['description'] = [
      '#type' => 'textarea',
      '#title' =>$this->t('Describe this job'),
      '#description' =>$this->t('Enter a description for his translation job'),
      '#default_value' => $job->getSetting('description') ? $job->getSetting('description') : '',
    ];

    return $settings;
  }

  /**
   * Validates the due at datetime element.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return void
   */
  public static function validateDueAt(array $element, FormStateInterface $formState) {
    $values = $formState->getValues();
    $due_at = NestedArray::getValue($values, $element['#parents']);
    if ($due_at['object'] < (new DrupalDateTime())) {
      $formState->setError($element, t('Due dat should be set to a future date.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    // If the job is finished, it's not possible to import translations anymore.
    if (!$job->isActive()) {
      return parent::checkoutInfo($job);
    }

    $form = [
      '#type' => 'fieldset',
      '#title' => t('Fetch translations'),
    ];

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Pull translations'),
      '#submit' => [
        [$this, 'pullTranslations'],
      ],
    );

    return $form;
  }

  /**
   * Calback for the pull translations button.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function pullTranslations(array $form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    /** @var \Drupal\tmgmt\JobInterface $job */
    $job = $form_state->getFormObject()->getEntity();
    $translator = $job->getTranslator();

    /** @var \Drupal\tmgmt_xillio\TapiccConnector $api_connector */
    $api_connector = \Drupal::service('tmgmt_xilio.tapicc_connector');
    $api_connector->configure($translator);

    try {
      /** @var \Drupal\tmgmt\RemoteMappingInterface $remote_mapping */
      foreach ($job->getRemoteMappings() as $remote_mapping) {
        $task_id = $remote_mapping->getRemoteIdentifier3();
        $task_status = $api_connector->checkTaksStatus($task_id);

        if (isset($task_status['status']) && in_array($task_status['status'], ['completed', 'completed-with-warnings'])) {
          $translator->getPlugin()->addTranslation($remote_mapping);
          $messenger->addMessage(t('Translation pulled successfully'));
        }
        elseif (isset($task_status['status']) && in_array($task_status['status'], ['cancelled', 'failed'])) {
          $job_item = $remote_mapping->getJobItem();
          $job_item->abortTranslation();
          $job->addMessage(t('Job item @item was aborted because the service marked is as @status', [
            "@item" => $job_item->label(),
            "@status" => $task_status['status'],
          ]));
        }
        else {
          $messenger->addMessage(t('Translations are not yet completed. Please retry later'));
        }
      }
    }
    catch (\Exception $e) {
      $job->addMessage(t('An error occurred when pulling translations: @error', [
        '@error' => $e->getMessage(),
      ]));
    }

  }

}
