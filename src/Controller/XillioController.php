<?php

namespace Drupal\tmgmt_xillio\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\JobItemInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Xillio routes.
 */
class XillioController extends ControllerBase {

  /**
   * Receives the task updated notification.
   */
  public function notifyTaskUpdate(Request $request) {
    $request_content = json_decode($request->getContent(), TRUE);
    if (isset($request_content['task']['status']) && $request_content['task']['status'] == 'completed') {
      $task = $request_content['task'];
      $mapping_candidates = RemoteMapping::loadByRemoteIdentifier(NULL, NULL, $task['id']);
      /** @var \Drupal\tmgmt\Entity\RemoteMapping $mapping */
      $mapping = array_shift($mapping_candidates);

      if (empty($mapping)) {
        return new Response('No job items found for this task.', 400);
      }

      $job_item = $mapping->getJobItem();
      if ($job_item->getState() == JobItemInterface::STATE_ACTIVE) {
        /** @var \Drupal\tmgmt_xillio\Plugin\tmgmt\Translator\XillioTranslator $plugin */
        $plugin = $job_item->getTranslatorPlugin();
        $plugin->addTranslation($mapping);
      }
    }
    return new Response(NULL, 202);
  }

}
