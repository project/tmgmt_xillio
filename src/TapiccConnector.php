<?php

namespace Drupal\tmgmt_xillio;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\Entity\Translator;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use ZipArchive;

/**
 * Connects to the Tapicc rest api provided by Xillio.
 */
Class TapiccConnector {

  /**
   * The tapicc api version.
   */
  const API_VERSION = '1.0.0-beta';

  /**
   * The key used to store the auth token in the keyvalue store.
   */
  const KEYVALUE_PREFIX = 'tmgmt_xillio_auth_token_';

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The service url.
   *
   * @var string
   */
  protected $serviceUrl = '';

  /**
   * The access token.
   *
   * @var string
   */
  protected $accessToken = '';

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Keyvalue store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $expirableKeyValue;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The key value store key.
   *
   * @var string|null
   */
  protected $keyValueKey = NULL;

  /**
   * Constructs the service.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactory $keyvalue_factory
   *   The keyvalue factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, KeyValueExpirableFactory $keyvalue_factory, FileSystemInterface $file_system) {
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('tmtmt_xillio');
    $this->expirableKeyValue = $keyvalue_factory->get('tmgmt_xillio');
    $this->fileSystem = $file_system;
  }

  /**
   * Set the service url.
   *
   * @param string $url
   *   The url.
   *
   * @return \Drupal\tmgmt_xillio\TapiccConnector
   */
  public function setServiceUrl($url) {
    $this->serviceUrl = $url;
    return $this;
  }

  /**
   * Set the access token.
   *
   * @param string $username
   *   The username string.
   * @param string $password
   *   The password string.
   *
   * @return \Drupal\tmgmt_xillio\TapiccConnector
   *   This object, after getting and setting the token.
   *
   * @throws \Exception
   */
  public function setAuthToken($username, $password) {
    $this->accessToken = $this->getAuthToken($username, $password);
    return $this;
  }

  /**
   * Configure the connector.
   *
   * @param \Drupal\tmgmt\Entity\Translator $translator
   *   The translator object.
   *
   * @return \Drupal\tmgmt_xillio\TapiccConnector
   *   The connector object.
   * @throws \Exception
   */
  public function configure(Translator $translator) {
    if (!$translator->getSettings()) {
      throw new \Exception('Missing configuration parameters');
    }

    $this->setServiceUrl($translator->getSetting('service_url'))
      ->setAuthToken($translator->getSetting('username'), $translator->getSetting('password'));

    return $this;
  }

  /**
   * Get the access token.
   *
   * @param string $username
   *   The username string.
   * @param string $password
   *   The password string.
   * @param bool $refresh
   *   Ignore keyvalue store and fetch the token via api.
   *
   * @return string|false
   *   The auth token string or false.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAuthToken($username, $password, $refresh = FALSE) {
    // Set the keyvalue key, so it can be deleted if anything goes wrong.
    $this->keyValueKey = $this->generateKeyvalueKey($username, $password);

    if ($refresh || !$this->expirableKeyValue->get($this->keyValueKey)) {
      $query = [
        'grant_type' => 'password',
        'client_id' => 'app',
        'username' => $username,
        'password' => $password,
      ];

      $request_options = [
        RequestOptions::QUERY => $query,
        RequestOptions::HEADERS => ['Content-Type' => 'application/x-www-form-urlencoded;charset=ISO-8859-1'],
      ];

      $result = $this->doRequest('oauth/token', $request_options);
      $this->expirableKeyValue->setWithExpire($this->keyValueKey, $result['access_token'], $result['expires_in'] - 5*50);

      return $result['access_token'];
    }

    return $this->expirableKeyValue->get($this->keyValueKey);
  }

  /**
   * Generate the key for the keyvalue store.
   *
   * @param string $username
   *   The username.
   * @param $password
   *   The password.
   *
   * @return string
   *   The key.
   */
  protected function generateKeyvalueKey($username, $password) {
    return 'tmgmt_xillio_auth_token_' . md5($username . $password);
  }

  /**
   * Get projects.
   *
   * @return array
   *   The array with project details.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getProjects() {
    $result = $this->doRequest('api/tapicc/' . static::API_VERSION . '/projects', [], 'GET');

    if (!empty($result['content'])) {
      return $result['content'];
    }

    return [];
  }

  /**
   * Check if the service is reachable.
   *
   * @return array|false
   *   The result of the ping request.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function ping() {
    return $this->doRequest('api/system/ping', [], 'GET');
  }

  /**
   * Create a translation job.
   *
   * @param array $payload
   *   An associative array with keys:
   *   - 'projectId': project id provided by xillio
   *   - 'submitter': free form reference to a drupal user
   *   - 'name': a name for the job
   *   - 'description': a description for the job
   *   - 'dueAt': an ISO 8601 compliant due date
   *   - 'externalId': a local id for the translaiton job
   *
   * @return array|false
   *   The call result or false.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createJob(array $payload) {
    $request_options = [
      RequestOptions::BODY => json_encode($payload)
    ];

    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/jobs', $request_options);
  }

  /**
   * Create a translation task.
   *
   * @param array $payload
   *   An associative array with keys:
   *   - 'jobId': The id of the job provided by Xillio
   *   - 'name': A name for the translation task
   *   - 'type': The type of taks, normally 'translation'
   *   - 'sourceLanguage' => The language code of the source language
   *   - 'targetLanguage' => The language code of the target language
   *
   * @return array|false
   *   The result of the call or false.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createTask(array $payload) {
    $request_options = [
      RequestOptions::BODY => json_encode($payload)
    ];

    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/tasks', $request_options);
  }

  /**
   * Update task status.
   *
   * @param string $task_id
   *   The task id.
   * @param string $status
   *   The status to set: pending, confirmed, paused, canceled.
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function updateTaskStatus(string $task_id, string $status) {
    $request_options = [
      RequestOptions::BODY => json_encode((object) ['status' => $status]),
    ];
    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/tasks/' . $task_id, $request_options, 'PATCH');
  }

  /**
   * Check task status.
   *
   * @param string $task_id
   *   the task id.
   *
   * @return mixed
   *   Usually an array  with the job info.
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function checkTaksStatus(string $task_id) {
    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/tasks/' . $task_id, [], 'GET');
  }

  /**
   * Create webhook.
   *
   * @param array $payload
   *   (optional) An associative array with additional payload data.
   *
   * @return array|false
   *   The result of the call or false.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createWebhook(array $payload = []) {
    // We hardcode this for now, because only task related events are supported.
    $payload['eventType'] = 'taskUpdated';
    $payload['url'] = Url::fromRoute('tmgmt_xillio.notify_task_update')->setAbsolute()->toString();
    $payload['name'] = 'Task updated notification for ' . Url::fromRoute('<front>')->setAbsolute()->toString();

    $request_options = [
      RequestOptions::BODY => json_encode($payload)
    ];

    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/webhooks', $request_options);
  }

  /**
   * Get webhooks.
   *
   * @return array|false
   *   The result of the call or false.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWebhooks() {
    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/webhooks', [], 'GET');
  }

  /**
   * Get a webhook.
   *
   * @param string $webhook_id
   *   The webhook id.
   *
   * @return array
   *   The result.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWebhook(string $webhook_id) {
    return $this->doRequest('api/tapicc/' . static::API_VERSION . '/webhooks/' . $webhook_id, [], 'GET');
  }

  /**
   * Delete a webhook.
   *
   * @param string $webhook_id
   *   The webhook id.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deleteWebhook(string $webhook_id) {
    $this->doRequest('api/tapicc/' . static::API_VERSION . '/webhooks/' . $webhook_id, [], 'DELETE');
  }

  /**
   * @param array $input
   * @param string $content
   *
   * @return array|false
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function uploadInput(\stdClass $input, string $content) {
    // @todo Find out why just using the string as contents does not work.
    // Because of that, we need to create a tmp file and then delete it.
    $temp_file = 'temporary://' . $input->fileOriginalName;
    file_put_contents($temp_file, $content);

    $request_options[RequestOptions::MULTIPART] = [
      [
        'name' => 'input',
        'contents' => json_encode($input),
        'headers' => [
          'Content-Type' => 'application/json'
        ],
      ],
      [
        'name' => 'content',
        'contents' => fopen($temp_file, 'r'),
        'headers' => [
          'Content-Type' => 'application/xml'
        ],
      ],
    ];

    $request_options[RequestOptions::HEADERS]['Content-Type'] = '';
    $request_options[RequestOptions::HEADERS]['Accept'] = '*/*';
    $request_options[RequestOptions::HEADERS]['Accept-Encoding'] = 'gzip, deflate, br';

    $result = $this->doRequest('api/tapicc/' . static::API_VERSION . '/inputs/-combined', $request_options);
    $this->fileSystem->delete($temp_file);
    return $result;
  }

  /**
   * Download deliverables.
   *
   * @param string $task_id
   *   The task id.
   *
   * @return array|false
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function downloadDeliverables(string $task_id) {
    $request_options[RequestOptions::HEADERS]['Content-Type'] = '';
    $request_options[RequestOptions::HEADERS]['Accept'] = '*/*';
    $result = $this->doRequest('api/tapicc/' . static::API_VERSION . '/tasks/' . $task_id . '/downloaddeliverables', $request_options, 'GET');

    if (!$result) {
      return FALSE;
    }

    $deliverables = [];
    $tmp_zip_path = 'temporary://xillio_deliverables_' . $task_id . '.zip';
    file_put_contents($tmp_zip_path, $result);
    $zip = new ZipArchive();
    $zip->open($this->fileSystem->realpath($tmp_zip_path));
    $zip->extractTo('temporary://');

    for ($i = 0; $i < $zip->numFiles; $i++) {
      $xlf_file = $zip->getNameIndex($i);
      if (!is_string($xlf_file) || empty($xlf_file)) {
        continue;
      }

      $xlf_file_path = 'temporary://' . $xlf_file;
      $deliverables[$xlf_file] = file_get_contents($xlf_file_path);
      $this->fileSystem->delete($xlf_file_path);
    }

    $this->fileSystem->delete($tmp_zip_path);
    return $deliverables;
  }

  /**
   * Queries the service.
   *
   * @param string $path
   *   The service path.
   * @param array $options
   *   (optional) An array containing headers, query, body, ...
   * @param string $method
   *   (optional) The request method. Defaults to POST.
   *
   * @return mixed
   *   The result of the api call. The return type can vary based on the content
   *   type of the result. Ie. for json responses it will be an array (or
   *   whatever json_decode returns). For application/zip it will be the zip
   *   binary.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function doRequest($path, array $options = [], string $method = 'POST') {
    if (empty($this->serviceUrl)) {
      throw new \Exception('Service url not set');
    }

    // Set defaults.
    $default_options = [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ],
    ];

    if ($this->accessToken) {
      $default_options[RequestOptions::HEADERS]['Authorization'] = 'Bearer ' . $this->accessToken;
    }
    $request_options = NestedArray::mergeDeep($default_options, $options);

    // Filter out empty options, so calling methods can unset defaults by
    // setting empty values.
    $request_options = NestedArray::filter($request_options);

    $url = $this->serviceUrl . '/' . $path;
    try {
      $result = $this->httpClient->request($method, $url, $request_options);

      switch ($result->getHeader('Content-Type')[0]) {
        case 'application/zip':
          return $result->getBody()->getContents();

        default:
          return json_decode($result->getBody()->getContents(), TRUE);
      }
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
      // Try deleting the expirable key if we have config.
      // @todo Check if the reason was auth token expiration if possible.
      if ($this->keyValueKey) {
        $this->expirableKeyValue->delete($this->keyValueKey);
      }

      if (method_exists($e, 'getResponse')) {
        $response = $e->getResponse();

        if (!empty($response) && $body = $response->getBody()) {
          $data = json_decode($body->getContents(), TRUE);

          if (isset($data['message'])) {
            $message = $data['message'];
          }
          elseif (isset($data['error_description'])) {
            $message = $data['error_description'];
          }
        }
      }

      throw new \Exception($message);
    }
  }

}
