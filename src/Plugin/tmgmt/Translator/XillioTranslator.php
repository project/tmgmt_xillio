<?php

namespace Drupal\tmgmt_xillio\Plugin\tmgmt\Translator;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\RemoteMappingInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt_xillio\TapiccConnector;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\AvailableResult;

/**
 * Xillio translation plugin controller.
 *
 * @TranslatorPlugin(
 *   id = "xillio",
 *   label = @Translation("LocHub"),
 *   description = @Translation("Connects Drupal to LocHub platform, a translation middleware provided by Xillio."),
 *   ui = "Drupal\tmgmt_xillio\XillioTranslatorUi",
 *   logo = "icons/xillio.svg",
 * )
 */
class XillioTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The api connector.
   *
   * @var \Drupal\tmgmt_xillio\TapiccConnector
   */
  protected  $apiConnector;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected  $logger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected  $currentUser;

  /**
   * The xliff manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected  $xliffManager;

  /**
   * Constructs a XillioTranslator object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\tmgmt_xillio\TapiccConnector $api_connector
   *   The api connector.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $xliff_manager
   *   The xliff plugin manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, TapiccConnector $api_connector, LoggerChannelFactoryInterface $logger_factory, AccountInterface $current_user, PluginManagerInterface $xliff_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->apiConnector = $api_connector;
    $this->logger = $logger_factory->get('tmgmt_xillio');
    $this->currentUser = $current_user;
    $this->xliffManager = $xliff_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tmgmt_xilio.tapicc_connector'),
      $container->get('logger.factory'),
      $container->get('current_user'),
      $container->get('plugin.manager.tmgmt_file.format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [
      'service_url' => '',
      'username' => '',
      'password' => '',
      'project' => NULL,
      'webhook_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    try {
      $ping = $this->apiConnector
        ->configure($translator)
        ->ping();

      if ($ping) {
        return AvailableResult::yes();
      }
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }

    return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $job_items = $job->getItems();
    $translator = $job->getTranslator();

    /** @var DrupalDateTime $due_at */
    $due_at_values = $job->getSetting('due_at');
    $due_at = $due_at_values['object'];
    $due_at->setTimezone(new \DateTimeZone('UTC'));
    // @todo Is there a better way to get the 2021-09-27T12:07:48.575Z format?
    $due = $due_at->format('Y-m-d\TH:i:s\Z');

    try {
      $project_id = $translator->getSetting('project');
      $api_connector = $this->apiConnector->configure($translator);

      $user = User::load($this->currentUser->id());
      $job_result = $api_connector->createJob([
        'submitter' => $user->toUrl()->setAbsolute()->toString(),
        'name' => $job->label(),
        'description' => $job->getSetting('description'),
        'externalId' => $job->id(),
        'dueAt' => $due,
        'projectId' => $project_id,
      ]);

      $job->addMessage('Translation job @id created.', [
        '@id' => $job_result['id']
      ]);

      /** @var \Drupal\tmgmt\Entity\JobItem $job_item */
      foreach ($job_items as $job_item) {
        $task_result = $this->apiConnector->createTask([
          'jobId' => $job_result['id'],
          'name' => $job_item->label(),
          'type' => 'translation',
          'sourceLanguage' => $job->getSourceLangcode(),
          'targetLanguage' => $job->getTargetLangcode(),
        ]);

        /** @var \Drupal\tmgmt_file\Format\FormatInterface $xliff_converter */
        $xliff_converter = $this->xliffManager->createInstance('xlf');
        $conditions = ['tjiid' => ['value' => $job_item->id()]];
        $xliff = $xliff_converter->export($job, $conditions);

        $input = (object) [
          'name' => $job_item->label(),
          'taskId' => $task_result['id'],
          'language' => $job->getSourceLangcode(),
          'inputAs' => 'source',
          'fileOriginalName' => $this->prepareFileName($job_item->label()) . '.xlf',
          'mimeType' => 'application/xml',
          'size' => mb_strlen($xliff),
          'projectId' => $project_id,
        ];
        $input = $this->apiConnector->uploadInput($input, $xliff);
        $this->apiConnector->updateTaskStatus($task_result['id'], 'confirmed');

        /** @var \Drupal\tmgmt\Entity\RemoteMapping $remote_mapping */
        $remote_mapping = RemoteMapping::create([
          'tjid' => $job->id(),
          'tjiid' => $job_item->id(),
          'remote_identifier_1' => 'tmgmt_xillio',
          'remote_identifier_2' => $job_result['id'],
          'remote_identifier_3' => $task_result['id'],
          'remote_data' => [
            'inputId' => $input['id'],
          ],
        ]);
        $remote_mapping->save();

      }

      $job->submitted();
    }
    catch (\Exception $e) {
      $job->rejected('Job has been rejected with following error: @error', ['@error' => $e->getMessage()], 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job) {
    try {
      $this->apiConnector->configure($job->getTranslator());
      foreach ($job->getRemoteMappings() as $remote_mapping) {
        $task_id = $remote_mapping->getRemoteIdentifier3();
        $this->apiConnector->updateTaskStatus($task_id, 'cancelled');
      }
      parent::abortTranslation($job);
      return TRUE;
    }
    catch (\Exception $e) {
      $job->addMessage($this->t('There was an error while trying to abort the translation: @error', [
        '@error' => $e->getMessage(),
      ]));
    }
    return FALSE;
  }

  /**
   * Add translation to Job.
   *
   * @param \Drupal\tmgmt\RemoteMappingInterface $remote_mapping
   *
   * @throws \Drupal\tmgmt\TMGMTException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function addTranslation(RemoteMappingInterface $remote_mapping) {
    $task_id = $remote_mapping->getRemoteIdentifier3();
    $job = $remote_mapping->getJob();

    $this->apiConnector->configure($job->getTranslator());
    $deliverables = $this->apiConnector->downloadDeliverables($task_id);

    if (!empty($deliverables)) {
      /** @var \Drupal\tmgmt_file\Format\FormatInterface $xliff_converter */
      $xliff_converter = $this->xliffManager->createInstance('xlf');
      // @todo This should be always 1 item.
      $parsed_translation = $xliff_converter->import(array_shift($deliverables), FALSE);

      $job->addTranslatedData($parsed_translation, [], TMGMT_DATA_ITEM_STATE_TRANSLATED);
    }
  }

  /**
   * Prepares the filename.
   *
   * @param string $name
   *   The string that needs conversion.
   *
   * @return string
   *   The prepared string.
   */
  protected function prepareFileName($name) {
      // Punctuation characters that are allowed, but not as first/last character.
      $punctuation = '-_. ';

      $map = [
        '!\s+!' => '_',
        // Replace (groups of) whitespace characters.
        '!\s!' => '_',
        // Replace multiple dots.
        '!\.!' => '.',
        // Remove characters that are not alphanumeric or the allowed punctuation.
        "![^0-9A-Za-z$punctuation]!" => '',
      ];

      // Apply the regex replacements. Remove any leading or hanging punctuation.
      return trim(preg_replace(array_keys($map), array_values($map), $name), $punctuation);
  }

}
